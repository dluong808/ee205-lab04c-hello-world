///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello World II
///
/// This is a procedural Hello World program not  using namespace std
///
/// @Daniel Luong<dluong@hawaii.edu>
//  @ February 9 2021
////////////////////////////////////////////////////////////////////////////

#include <iostream>


int main(){
   std::cout<<"Hello World!" << std::endl;

   return 0;
}

