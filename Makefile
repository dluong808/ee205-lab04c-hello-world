# Build two C++ Hello World Programs



TARGETS= hello hello2

all: $(TARGETS)

hello: hello.cpp
	g++ -g -Wall -o hello hello.cpp

hello2: hello2.cpp
	g++ -g -Wall -o hello2 hello2.cpp

clean:
	rm -f $(TARGETS) *.o
